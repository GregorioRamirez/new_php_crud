FROM php:8.2-fpm-alpine3.18

RUN apk add nginx
RUN docker-php-ext-install mysqli
COPY . /var/www/website
COPY server.conf /etc/nginx/http.d/server.conf
EXPOSE 8080
CMD php-fpm -D;nginx -g "daemon off;"